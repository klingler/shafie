<?php /*
[ExtensionSettings]
DesignExtensions[]=shafie

[JavaScriptSettings]
FrontendJavaScriptList[]=jquery.tools.min.js
FrontendJavaScriptList[]=jquery.colorbox-min.js
FrontendJavaScriptList[]=jquery.swipebox.min.js
#FrontendJavaScriptList[]=bootstrap.min.js
FrontendJavaScriptList[]=hammer.min.js



[StylesheetSettings]
FrontendCSSFileList[]=style.css
FrontendCSSFileList[]=colorbox.css
FrontendCSSFileList[]=bootstrap.min.css
FrontendCSSFileList[]=swipebox.min.css




 */?>