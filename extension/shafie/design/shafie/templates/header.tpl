<div id="content_header">
      
      <img src="/extension/shafie/design/shafie/images/mobile_menu.png" width="38" id="mobile_menu_icon" />
<h1>AFSAR SONIA SHAFIE</h1>
<h3>FILMMAKER</h3>

<div id="menu">
{def $parent = fetch('content', 'node', hash('node_id', 2))
      $menuNodes = fetch('content', 'list', fetch('parent_node_id', 2,
                                                 'class_filter_array', array('article'),
                                                 'class_filter_type', 'include',
                                                 'sort_by', $parent.sort_array))}
{def $main_menu_ids = array(63, 64, 65, 71, 66)
     $index = 0
     $current = null
     $current_children = null
     $sub_index = 0}                     
         <ul id="menu-list">                                        
         {foreach   $main_menu_ids as $mid}
           {set $current = fetch('content', 'node', hash('node_id', $mid))
                $current_children = fetch('content', 'list', hash('parent_node_id', $mid,
                                                               'class_filter_array', array('article', 'folder'),
                                                               'class_filter_type', 'include',
                                                               'sort_by', $current.sort_array))
               $sub_index = 0}
               
           {if eq($mid, $pagedata.path_id_array.1)}
             <li class="selected_menu{if ne(4, $index)} menu_item_margin_right{/if}" onmouseover="menuHandler.showMenu({$mid})" onmouseout="menuHandler.hideMenu({$mid})"><a href={$current.url_alias|ezurl()}>{$current.name|wash}</a>
           {else}
          <li{if ne(4, $index)} class="menu_item_margin_right"{/if} onmouseover="menuHandler.showMenu({$mid})" onmouseout="menuHandler.hideMenu({$mid})"e><a href={$current.url_alias|ezurl()} >{$current.name|wash}</a>
           {/if}
           {if lt(0, $current_children|count)}<ul class="submenu" id="submenu_{$mid}">
                  {foreach $current_children as $ch}
                  <li class="submenu_item{if or(and(eq($pagedata.node_id, $mid), eq(0, $sub_index) ),eq($ch.node_id, $pagedata.path_id_array.2))} selected_submenu_item{/if}"><a href={$ch.url_alias|ezurl()}>{$ch.name|wash}</a></li>
                    {set $sub_index = $sub_index|inc}
                  {/foreach}
                  
             </ul>{/if}
            
             </li>
           {set $index = $index|inc}
          {/foreach}
   </ul>
   <div style="clear:both;height:7px;"></div>
</div>
</div>
<script type="text/javascript">
      {literal}

      
      

var menuHandler = null;

$(document).ready(function() {
  
  
  menuHandler = {
    showMenu:function(id) {
      $('#submenu_'+id).css('display', 'block');
      $('#submenu_'+id).css('visibility', 'visible');
      
    },
    hideMenu:function(id) {
      $('#submenu_'+id).css('visibility', 'hidden');
      $('#submenu_'+id).css('display', 'none');
    }
  }
  $('#mobile_menu_icon').click(function() {
     $('#menu-list').slideToggle(200); 
  })
      
      
  
});   
      
      
      
      {/literal}
</script>