<!DOCTYPE html>
<!--[if lt IE 9 ]><html class="unsupported-ie ie" lang="{$site.http_equiv.Content-language|wash}"><![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="{$site.http_equiv.Content-language|wash}"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="{$site.http_equiv.Content-language|wash}"><!--<![endif]-->
<head>
{def $basket_is_empty   = cond( $current_user.is_logged_in, fetch( shop, basket ).is_empty, 1 )
     $user_hash         = concat( $current_user.role_id_list|implode( ',' ), ',', $current_user.limited_assignment_value_list|implode( ',' ) )}

{include uri='design:page_head_displaystyles.tpl'}

{if is_set( $extra_cache_key )|not}
    {def $extra_cache_key = ''}
{/if}

{def $pagedata        = ezpagedata()
     $inner_column_size = $pagedata.inner_column_size
     $outer_column_size = $pagedata.outer_column_size


     $cached_id = first_set($pagedata.path_id_array.2, first_set($pagedata.path_id_array.1, first_set($pagedata.path_id_array.0, 0)) )
     
     $node_id_set = first_set($pagedata.node_id, 0)}

{cache-block keys=array( $cached_id )}
{def $pagestyle        = $pagedata.css_classes
     $locales          = fetch( 'content', 'translation_list' )
     $current_node_id  = $pagedata.node_id}

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--
<<<<<<< HEAD
     <script type="text/javascript" src="/extension/shafie/design/shafie/javascripts/jquery.tools.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="/extension/shafie/design/shafie/javascripts/jquery.colorbox-min.js" charset="utf-8"></script>
    <script type="text/javascript" src="/extension/shafie/design/shafie/javascripts/hammer.min.js" charset="utf-8"></script>
=======
    <script type="text/javascript" src="/extension/shafie/design/shafie/javascripts/jquery.tools.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="/extension/shafie/design/shafie/javascripts/jquery.swipebox.min.js" charset="utf-8"></script>
    
>>>>>>> swipe1
    -->
    
    

    {ezscript_load( 

        array( 

            ezini(  'JavaScriptSettings', 'FrontendJavaScriptList', 'design.ini' ) 

        ) 

    )}
     



{include uri='design:page_head.tpl'}
{include uri='design:page_head_style.tpl'}
{include uri='design:page_head_script.tpl'}

</head>
<body>

  <!-- Complete page area: START -->
<div id="body" class="content">
     
{include uri="design:header.tpl"}
{if or(is_set($pagedata.path_id_array[1], is_set($pagedata.path_id_array[2])))}
   {def $reverse = $pagedata.path_id_array|reverse
         $image_node = null}  
    {foreach $reverse as $rid}
      {set $image_node = fetch('content', 'node', hash('node_id', $rid))}
      {if $image_node.data_map.image.has_content}
        {break}
       {/if}
    {/foreach}
   
   {if $image_node.data_map.image.has_content}
     <img src={$image_node.data_map.image.content.original.url|ezroot}  id="head-photo"/>
     
     
  {else}
  
   <img src={'photo-home.jpg'|ezimage()} id="head-photo">
   {/if}
{else}
   <img src={'photo-home.jpg'|ezimage()} id="head-photo">
{/if}
{/cache-block}
{cache-block keys=array($node_id_set, first_set($pagedata.path_id_array.1, 0))}
   {include uri="design:path.tpl"}
 
  {/cache-block}
            {$module_result.content}
          
            {cache-block keys=array( $cached_id )}

            <div id="home-bottom-imge-container">
            <img id="home-bottom-image" src={'blumen.png'|ezimage()} />
           </div>
         <div id="footer">
          
          <div id="copyright">
               
              {def $footer = fetch('content', 'node', hash('node_id', 60))}
              {attribute_view_gui attribute=$footer.data_map.body}
          </div>
          <ul id="social">
           </ul>    
               
          </div>
          
         </div>

{/cache-block}


{* This comment will be replaced with actual debug report (if debug is on). *}
<!--DEBUG_REPORT-->

</div>
</body>
</html>
