 <div id="article-title-bar">
 {if lt(0, $pagedata.path_id_array.1)}
   {def $n = fetch('content', 'node', hash('node_id', $pagedata.path_id_array.1))}
   {if $n.children|count}
       
      
       {def $childNodes = fetch('content', 'list', hash('parent_node_id', $pagedata.path_id_array.1,
                                                   'sort_by', $n.sort_array))
            $selected_node = 0}
           
        {if gt(3 , $pagedata.path_id_array|count)}
          {set $selected_node = $childNodes.0.node_id}
        {/if}
                                    
           <ul id="page_sub_menu">
        {foreach $childNodes as $c}
           {if or(eq($selected_node, $c.node_id), $pagedata.path_id_array|contains($c.node_id))}
           <li class="selectede_subitem"><a href={$c.url_alias|ezurl()} >{$c.name|wash}</a></li>
           {else}
           
           
          <li><a href={$c.url_alias|ezurl()} >{$c.name|wash}</a></li>
           {/if}
        {/foreach}
           </ul>
   {else}
       {$n.name|wash}
   {/if}

 
 {/if}
 <div style="clear:both;"></div>
 </div>