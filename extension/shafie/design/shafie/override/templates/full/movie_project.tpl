<div class="movie_content">
    
    <div class="embedded_movie_container">
          <h2>{$node.name|wash|trim}</h2>
    
    
    {if $node.data_map.image.has_content}
		{attribute_view_gui attribute=$node.data_map.image image_class="movie_image"}
       
       {if $node.data_map.share_code.has_content}
	    <div id="movie_placeholder_{$node.node_id}" class="movie_placeholder_class">
       
		</div>
	     {def  $sharecode = getYoutubeShareCode($node.data_map.share_code.content)}
		 
	     {literal}
	     <script type="text/javascript">
			
			$('#image_attriubte_{/literal}{$node.data_map.image.id}{literal}').click( function() {
				var ifr = '<iframe width="100%" height="100%" src="{/literal}{$sharecode}{literal}" frameborder="0" allowfullscreen id="iframe_{/literal}{$node.node_id}{literal}"></iframe>';
				
				
				$('#image_attriubte_{/literal}{$node.data_map.image.id}{literal}').hide();
				$('#movie_placeholder_{/literal}{$node.node_id}{literal}').show();
				$('#movie_placeholder_{/literal}{$node.node_id}{literal}').html(ifr);
				
				
			});
			
		 </script>
		 {/literal}
	     
	   {/if}
		
    {elseif $node.data_map.share_code.has_content}


{def  $sharecode = getYoutubeShareCode($node.data_map.share_code.content)}


  
<iframe width="100%" height="100%" src="{$sharecode}" frameborder="0" allowfullscreen></iframe>
  
  {else}
    <div style="width:496"></div>
  {/if}
  
{def $pics = $node.data_map.images
     $currentPicture  = null
     $count = 0}

<div class="movie_teaser_container">
{foreach $pics.content.relation_list as $p}
  {set $currentPicture = fetch('content', 'node', hash('node_id', $p.node_id))}
  <a class="small_movie_picture teaser_group_{$node.node_id}" rel="#gi{$node.node_id}" title="{$currentPicture.name|wash}" href={$currentPicture.object.data_map.image.content.movie_image.url|ezroot}>
   <input type="hidden" name="image_width" value="{$currentPicture.data_map.image.content.galleryfull.width}" class="image_width" />
   <input type="hidden" name="image_height" value="{$currentPicture.data_map.image.content.galleryfull.height}" class="image_height" />
   <img src={$currentPicture.object.data_map.image.content.movie_teaser.url|ezroot} />
  </a>
  
  

  
  
  {set $count = $count|inc}
  {if ge($count, 5)}
    {break}
  {/if}
{/foreach}
</div>
<div style="clear:left"></div>
<script type="text/javascript" >
     {literal}
     
$(document).ready(function() {
 

	$('a.teaser_group_{/literal}{$node.node_id}{literal}').swipebox({rel:'#gi{/literal}{$node.node_id}{literal}', hideBarsDelay : 0});
});
     
     
     {/literal}
</script>



</div>
<div class="movie_descitption_container">
    {if $node.data_map.production_year.has_content}
       <h2>{$node.data_map.production_year.content}</h2>
    {/if}
    <b>{attribute_view_gui attribute=$node.data_map.head_line}</b>
    {attribute_view_gui attribute=$node.data_map.description}
  <div class="movie-ext-information">  
    {if $node.data_map.flyer.has_content}
    <h4>Flyer</h4>
    {attribute_view_gui attribute=$node.data_map.flyer}
    {/if}
     {if $node.data_map.textfile.has_content}
   <h4>Text</h4>
    {attribute_view_gui attribute=$node.data_map.textfile}
    {/if}
     {if $node.data_map.link.has_content}
     <h4>Link:</h4>
    {attribute_view_gui attribute=$node.data_map.link}
    {/if}
  </div>
</div>
</div>
<div style="clear:both;" class="movie_project_bottom"></div>
