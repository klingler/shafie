{def $child_articles_count = fetch('content', 'list_count', hash('parent_node_id', $node.node_id,
                                          'class_filter_array', array('article'),
                                           'class_filtet_type', 'include'))}
                                           
{if lt(0, $child_articles_count)}
  {def $child_articles = fetch('content', 'list', hash('parent_node_id', $node.node_id,
                                          'class_filter_array', array('article'),
                                           'class_filtet_type', 'include',
                                           'sort_by', $node.sort_array))}
  {node_view_gui content_node=$child_articles.0 view="full"}

{else}
  
  <div class="article-body{if eq($node.node_id, 63)} column_body{/if}">
{attribute_view_gui attribute=$node.data_map.body}
</div>
  
   {*if eq($node.node_id, 63)*}
  <div id="home-bottom-container">
   
    <div id="home-bottom-container-text">
   {attribute_view_gui attribute=$node.data_map.intro}
    <div id="quote_author">{attribute_view_gui attribute=$node.data_map.quote_author}</div>
    </div>
   
    
  </div>

   {*/if*}
{/if}
