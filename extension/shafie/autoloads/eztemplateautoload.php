<?php

/**
 * Look in the operator files for documentation on use and parameters definition.
 *
 * @var array $eZTemplateOperatorArray
 */
$eZTemplateOperatorArray = array();

$eZTemplateOperatorArray[] = array( 'script'         => 'extension/shafie/classes/shafiefunctions.php',
                                    'class'          => 'ShafieFunctions',
                                    'operator_names' => array( 'getYoutubeShareCode' ) );


?>
