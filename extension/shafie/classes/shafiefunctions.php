<?php

/**
 * eZTagsTemplateFunctions class implements eztags tpl operator methods
 *
 */
class ShafieFunctions
{
    /**
     * Return an array with the template operator name.
     *
     * @return array
     */
    function operatorList()
    {
        return array( 'getYoutubeShareCode' );
    }

    /**
     */
    function namedParameterPerOperator()
    {
        return true;
    }

    /**
     * Returns an array of named parameters, this allows for easier retrieval
     * of operator parameters. This also requires the function modify() has an extra
     * parameter called $namedParameters.
     *
     * @return array
     */
    function namedParameterList()
    {
        return array( 'getYoutubeShareCode' => array( 'share_code' => array( 'type'     => 'string',
                                                                          'required' => true,
                                                                          'default'  => '' ) ) );
    }

    /**

     */
    function modify( $tpl, $operatorName, $operatorParameters, $rootNamespace, $currentNamespace, &$operatorValue, $namedParameters )
    {
        switch ( $operatorName )
        {
            case 'getYoutubeShareCode':
            {
                $operatorValue = self::getShareCode( $namedParameters['share_code'] );
            } 
        }
    }
    public static function getShareCode($share_code) {
        
        //$exp = explode('/', $share_code);
        //return array_pop($exp);
        return $share_code;
        
    }

  
}
?>
